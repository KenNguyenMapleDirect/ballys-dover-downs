<?php

namespace App\Http\Controllers\Auth;

use App\Data;
use App\Flipbook;
use App\Http\Controllers\Controller;
use App\Mail\WelcomeMail;
use Illuminate\Http\Request;
use App\User;
use Hash;
use Illuminate\Validation\ValidationException;
use Mail;

class RegisterController extends Controller
{
    public function register()
    {

        return view('auth.register');
    }

    public function storeUser(Request $request)
    {
        $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'user_name' => 'required|string|max:255|unique:users',
            'birthday' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'BAC_Account_Combined' => 'required|string|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
            'password_confirmation' => 'required',
        ]);

        //check if user register right birthday and BAC_Account_Combined
        $checkAccount = Data::where('BAC_Account_Combined', $request->BAC_Account_Combined)
            ->where('BAC_DOB', $request->birthday)->first();
        if (!$checkAccount) {
            throw ValidationException::withMessages(['No_Match_Up' => 'Your Account Combined ID or Birthday does not match our records
. Please contact guest services.']);
        }
        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'user_name' => $request->user_name,
            'email' => $request->email,
            'role_id' => 2,
            'password' => Hash::make($request->password),
            'BAC_Account_Combined' => $request->BAC_Account_Combined,
            'BAC_Account_Number' => $checkAccount->BAC_Account_Number,
            'BAC_DOB' => $request->birthday,
            'Email_Opt_In' => 'Y',
        ]);
        $user->sendEmailVerificationNotification();
        return redirect('admin/login')->with('message', 'Register Completed! Please check your mailbox and confirm your email.');
    }

}
